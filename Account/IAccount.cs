﻿using System;

namespace Account
{
    public interface IAccount
    {
        CreateAccountResponse CreateAccount(CreateAccountRequest request);
        LoginResponse Login(LoginRequest request);
        ChangePaswordResponse ChangePassword(ChangePaswordRequest request);
        GetAccountInfoResponse GetAccount(GetAccountInfoRequest request);
        UpdateAccountResponse UpdateAccount(UpdateAccountRequest request);
        LogoutResponse Logout(LogoutRequest request);
    }
}
