﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Account.Exceptions
{
    public class UserEmailAdressAlreadyUsingException : BaseException
    {
        public override int ErrorCode => 1004;
    }
}
