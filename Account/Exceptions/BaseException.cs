﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account.Exceptions
{
    public abstract class BaseException : Exception
    {
        public abstract int ErrorCode { get; }
    }
}
