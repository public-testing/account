﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account.Exceptions
{
    public class PasswordsNotMatchedException : BaseException
    {
        public override int ErrorCode => 1003;
    }
}
