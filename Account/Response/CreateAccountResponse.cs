﻿namespace Account
{
    public class CreateAccountResponse : ResponseBase
    {
        public int AccountID { get; set; }
        public string NewPassword { get; set; }
    }
}