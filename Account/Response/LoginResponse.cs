﻿namespace Account
{
    public class LoginResponse : ResponseBase
    {
        public string Token { get; set; }
    }
}