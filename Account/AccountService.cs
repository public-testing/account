﻿using Account.Exceptions;
using System;
using System.Linq;

namespace Account
{
    public class AccountService : IAccount
    {
        public ChangePaswordResponse ChangePassword(ChangePaswordRequest request)
        {
            if (request == null) throw new RequestNullException();

            ChangePaswordResponse response = new ChangePaswordResponse();
            using (var context = new SandBoxDBContext())
            {
                Session session = context.Sessions.Single(c => c.Token == request.Token);
                if (session != null)
                {
                    User user = context.Users.SingleOrDefault(c => c.ID == session.UserID);
                    if (user.Password == request.CurrentPassword)
                    {
                        if (request.NewPassword == request.NewPasswordConfirm)
                        {
                            user.Password = request.NewPassword;

                            context.SaveChanges();

                            response.IsSuccess = true;
                        }
                        else
                        {
                            throw new PasswordsNotMatchedException();
                        }
                    }
                    else
                    {
                        throw new CurrentPasswordInvalidException();
                    }
                }
                else
                {
                    throw new InvalidTokenException();
                }
            }
            return response;
        }
        public CreateAccountResponse CreateAccount(CreateAccountRequest request)
        {
            if (request == null) throw new RequestNullException();

            CreateAccountResponse response = new CreateAccountResponse();
            using (var context = new SandBoxDBContext())
            {
                if (!context.Users.Any(c => c.Email == request.EMail))
                {
                    var user = new User()
                    {
                        Email = request.EMail,
                        Name = request.Name,
                        LastName = request.LastName,
                        Password = Utility.RandomNumber(1000, 9999).ToString()
                    };
                    context.Users.Add(user);

                    context.SaveChanges();

                    response.AccountID = user.ID;
                    response.IsSuccess = true;
                    response.NewPassword = user.Password;
                }
                else
                {
                    throw new UserEmailAdressAlreadyUsingException();
                }
            }
            return response;
        }
        public GetAccountInfoResponse GetAccount(GetAccountInfoRequest request)
        {
            if (request == null) throw new RequestNullException();

            GetAccountInfoResponse response = new GetAccountInfoResponse();
            using (var context = new SandBoxDBContext())
            {
                Session session = context.Sessions.SingleOrDefault(c => c.Token == request.Token);
                if (session != null)
                {
                    User user = context.Users.SingleOrDefault(c => c.ID == session.UserID);

                    response.Name = user.Name;
                    response.LastName = user.LastName;

                    response.IsSuccess = true;
                }
                else
                {
                    throw new InvalidTokenException();
                }
            }
            return response;
        }
        public LoginResponse Login(LoginRequest request)
        {
            if (request == null) throw new RequestNullException();

            LoginResponse response = new LoginResponse();
            using (var context = new SandBoxDBContext())
            {
                User user = context.Users.SingleOrDefault(c => c.Email == request.EMail && c.Password == request.Password);
                if (user != null)
                {
                    var session = new Session()
                    {
                        UserID = user.ID,
                        ExpiredDate = DateTime.Now.AddHours(1),
                        Token = Utility.CalculateMD5Hash($"{Guid.NewGuid().ToString()}{DateTime.Now.Ticks.ToString()}{user.ID}")
                    };
                    context.Sessions.Add(session);

                    context.SaveChanges();

                    response.Token = session.Token;
                    response.IsSuccess = true;
                }
                else
                {
                    throw new InvalidCredentialsException();
                }
            }
            return response;
        }
        public LogoutResponse Logout(LogoutRequest request)
        {
            if (request == null) throw new RequestNullException();

            LogoutResponse response = new LogoutResponse();
            using (var context = new SandBoxDBContext())
            {
                Session session = context.Sessions.SingleOrDefault(c => c.Token == request.Token);
                if (session != null)
                {
                    context.Sessions.Remove(session);
                    context.SaveChanges();

                    response.IsSuccess = true;
                }
                else
                {
                    throw new InvalidTokenException();
                }
            }
            return response;
        }
        public UpdateAccountResponse UpdateAccount(UpdateAccountRequest request)
        {
            if (request == null) throw new RequestNullException();

            UpdateAccountResponse response = new UpdateAccountResponse();
            using (var context = new SandBoxDBContext())
            {
                Session session = context.Sessions.Single(c => c.Token == request.Token);
                if (session != null)
                {
                    User user = context.Users.SingleOrDefault(c => c.ID == session.UserID);
                    user.LastName = request.LastName;
                    user.Name = request.Name;

                    context.SaveChanges();

                    response.IsSuccess = true;
                }
                else
                {
                    throw new InvalidTokenException();
                }
            }

            return response;
        }
    }
}
