﻿namespace Account
{
    public class ChangePaswordRequest : RequestBase
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string NewPasswordConfirm { get; set; }
    }
}