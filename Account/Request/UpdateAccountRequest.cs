﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account
{
    public class UpdateAccountRequest : RequestBase
    {
        public string Name { get; set; }
        public string LastName { get; set; }
    }
}
