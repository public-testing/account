﻿namespace Account
{
    public class CreateAccountRequest
    {
        public string EMail { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
    }
}