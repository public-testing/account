﻿using SimpleInjector;


namespace Account
{
    public class ServiceContainer 
    {
        public static Container Container { get; internal set; }

        public static void Build()
        {
            Container = new Container();
            Container.Register<IAccount, AccountService>();
        }
        public static void Verify()
        {
            if (ServiceContainer.Container != null)
                ServiceContainer.Container.Verify();
        }
        public static TService GetService<TService>()  where TService : class
        {
            if (ServiceContainer.Container != null)
                return Container.GetInstance<TService>();
            else
                return default(TService);
        }

        public static void Dispose()
        {
            if (ServiceContainer.Container != null)
                ServiceContainer.Container.Dispose();
        }
    }
}
